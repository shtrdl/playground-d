import { Component, MeshComponent, Property, TextComponent } from '@wonderlandengine/api';
import { CursorTarget, HowlerAudioSource } from '@wonderlandengine/components';
import { hapticFeedback } from './haptic-feedback.js';

export class ButtonComponentActiveText extends Component {
  static TypeName = 'toggle-active-text';
  static Properties = {
    sourceObject: Property.object(),
    hoverMaterial: Property.material(),
    targetTextObject: Property.object(),
    targetTextObject2: Property.object(),
    targetMeshObject: Property.object(),

    // hoverMaterial: Property.material(),
  };

  static onRegister(engine) {
    engine.registerComponent(HowlerAudioSource);
    engine.registerComponent(CursorTarget);
  }

  /* Position to return to when "unpressing" the button */

  start() {
    this.mesh = this.sourceObject.getComponent(MeshComponent);
    this.defaultMaterial = this.mesh.material;

    this.target = this.object.getComponent(CursorTarget) || this.object.addComponent(CursorTarget);

    this.soundClick = this.object.addComponent(HowlerAudioSource, {
      src: 'sfx/click.wav',
      spatial: true,
    });
    this.soundUnClick = this.object.addComponent(HowlerAudioSource, {
      src: 'sfx/unclick.wav',
      spatial: true,
    });

    // toggled state
    this.toggled = false;
    this.hover = false;

    // Get target object property
    this.targetTextComponent = this.targetTextObject.getComponent(TextComponent);

    if (this.targetTextObject2) {
      this.targetTextComponent2 = this.targetTextObject2.getComponent(TextComponent);
      this.isTargetActive2 = this.targetTextComponent2.active;
    }

    this.isTargetActive = this.targetTextComponent.active;

    this.labelAttributes = {
      vytahy: 'Výtahy a klimatizace',
      komin: 'Komín',
      vez: 'Věž',
      stresni: 'Střešní nástavba',
      hlavni: 'Hlavní část objektu',
    };

    this.targetMeshComponent = this.targetMeshObject.getComponent(MeshComponent);
  }

  onActivate() {
    this.target.onHover.add(this.onHover);
    this.target.onUnhover.add(this.onUnhover);
    this.target.onDown.add(this.onDown);
    this.target.onClick.add(this.onClick);
    this.target.onUp.add(this.onUp);
  }

  onDeactivate() {
    this.target.onHover.remove(this.onHover);
    this.target.onUnhover.remove(this.onUnhover);
    this.target.onDown.remove(this.onDown);
    this.target.onClick.remove(this.onClick);
    this.target.onUp.remove(this.onUp);
  }

  /* Called by 'cursor-target' */
  onHover = (_, cursor) => {
    hapticFeedback(cursor.object, 0.2, 50);
    this.hover = true;

    // get apropriate text
    this.targetTextComponent.text = this.labelAttributes[this.sourceObject.name.split('_')[0]];
    this.targetTextComponent2.text = this.targetTextComponent.text;
    this.targetTextComponent.active = true;
    this.targetTextComponent2.active = true;

    this.mesh.material = this.hoverMaterial;

    if (cursor.type === 'finger-cursor') {
      this.onDown(_, cursor);
    }
  };

  /* Called by 'cursor-target' */

  onDown = (_, cursor) => {
    return;
  };

  onClick = (_, cursor) => {
    // toggles material on given target

    this.toggled = !this.toggled;

    if (this.toggled) {
      //   targetTextObject changes
      this.targetMeshComponent.material = this.hoverMaterial;

      // button object changes
      this.soundClick.play();

      if (this.object.name.endsWith('hlavni')) {
        this.object.scaleLocal([1.1, 1.1, 1.1]);
      } else {
        this.object.scaleLocal([1.2, 1.2, 1.2]);
      }

      hapticFeedback(cursor.object, 1.0, 20);
      this.mesh.material = this.hoverMaterial;
    } else {
      // on up implemented here

      // targetTextObject changes
      this.targetMeshComponent.material = this.defaultMaterial;

      // button object changes
      this.soundUnClick.play();
      this.object.resetScaling();
      hapticFeedback(cursor.object, 0.7, 20);

      this.mesh.material = this.defaultMaterial;
    }
  };

  /* Called by 'cursor-target' */
  onUp = (_, cursor) => {
    return;
  };

  /* Called by 'cursor-target' */
  onUnhover = (_, cursor) => {
    hapticFeedback(cursor.object, 0.2, 50);
    this.hover = false;
    this.targetTextComponent.active = false;
    this.targetTextComponent2.active = false;

    if (this.toggled) {
      this.mesh.material = this.hoverMaterial;
    } else {
      this.mesh.material = this.defaultMaterial;
    }

    if (cursor.type === 'finger-cursor') {
      this.onUp(_, cursor);
    }
  };
}
