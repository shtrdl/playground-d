import { InputComponent } from '@wonderlandengine/api';

/**
 * Helper function to trigger haptic feedback pulse.

 * @param {Object} object An object with 'input' component attached
 * @param {number} strength Strength from 0.0 - 1.0
 * @param {number} duration Duration in milliseconds
 */
export function hapticFeedback(object, strength, duration) {
  if (object.name === 'Camera Non XR') {
    return;
  }

  let input = object.getComponent(InputComponent);

  if (input && input.xrInputSource) {
    // console.log('found gamepad');
    const gamepad = input.xrInputSource.gamepad;
    if (gamepad && gamepad.hapticActuators) gamepad.hapticActuators[0].pulse(strength, duration);
  }
}
